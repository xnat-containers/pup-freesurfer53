# PUP Docker Image

##Example input is located in:

`/data/nil-bluearc/benzinger2/Sarah/PUP_stuff/example_for_docker`

(The previously run pet_proc folder needs to be deleted before PUP can be run using the below command). 


##Example command:

`docker run -v /data/nil-bluearc/benzinger2/Sarah/PUP_stuff/example_for_docker:/input pup /bin/sh -c 'petproc /input/params/3011005_T80_v28.params > pet_docker_test.petlog'`