# Use CentOS 6.10 as the base image
FROM centos:6.10

# Begin freesurfer installation
# Update packages and download Freesurfer5.3.0
RUN yum -y update
RUN yum -y --enablerepo=extras install epel-release
RUN yum -y install wget tar tcsh libgomp netcdf netcdf-devel perl perl-core bc patch
RUN mkdir -p /freesurfer53
RUN wget -N -qO- https://surfer.nmr.mgh.harvard.edu/pub/dist/freesurfer/5.3.0/freesurfer-Linux-centos6_x86_64-stable-pub-v5.3.0.tar.gz | tar -xzv -C /freesurfer53 \
    --exclude='freesurfer/trctrain' \
    --exclude='freesurfer/subjects/fsaverage_sym' \
    --exclude='freesurfer/subjects/fsaverage3' \
    --exclude='freesurfer/subjects/fsaverage4' \
    --exclude='freesurfer/subjects/fsaverage5' \
    --exclude='freesurfer/subjects/fsaverage6' \
    --exclude='freesurfer/subjects/cvs_avg35' \
    --exclude='freesurfer/subjects/cvs_avg35_inMNI152' \
    --exclude='freesurfer/subjects/bert' \
    --exclude='freesurfer/subjects/V1_average' \
    --exclude='freesurfer/average/mult-comp-cor' \
    --exclude='freesurfer/lib/cuda' \
    --exclude='freesurfer/lib/qt'

# Copy in the license for Freesurfer
COPY .license /freesurfer53/freesurfer/.license

# begin PUP compilation

COPY pup_files/4dfp/ /4dfp_install_files/
COPY pup_files/4dfp/ecat/ /ecat
COPY pup_files/src/ /pup/src
COPY pup_files/scripts/ /pup/scripts
COPY pup_files/matlabcode/ /pup/matlabcode
COPY pup_files/ROIs /pup

# 4dfp tools setup

ENV NILSRC=/4dfp_install_files
ENV RELEASE=/4dfp
ENV OSTYPE=linux

RUN ./4dfp_install_files/make_nil-tools.csh

# ecat setup

RUN tar -xvzf /ecat/libecat7-1.5.tgz
RUN /bin/tcsh -c 'cd /libecat7-1.5; ./configure; make; make install; /sbin/ldconfig'

# PUP build step
RUN /bin/tcsh -c 'cd /pup/src; make release'

RUN mkdir -p /input
RUN mkdir -p /output

# Configure Freesurfer environment
ENV SUBJECTS_DIR /output
ENV FREESURFER_HOME /freesurfer53/freesurfer
ENV PERL5LIB /freesurfer53/freesurfer/mni/lib/perl5/5.8.5
ENV MNI_PERL5LIB /freesurfer53/freesurfer/mni/lib/perl5/5.8.5

# Configure Freesurfer and PUP path
ENV PATH /freesurfer53/freesurfer/bin:/freesurfer53/freesurfer/fsfast/bin:/freesurfer53/freesurfer/tktools:/freesurfer53/freesurfer/mni/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/pup/scripts/:/4dfp

# Configure cshrc to source SetUpFreeSurfer.csh
RUN /bin/tcsh -c 'echo -e "source $FREESURFER_HOME/FreeSurferEnv.csh &>/dev/null" >> /root/.cshrc '

WORKDIR /input